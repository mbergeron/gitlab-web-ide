FROM node:alpine

EXPOSE 5000

RUN npm install -g nodemon
COPY dockerscripts/app-entrypoint.sh /usr/bin/

ENTRYPOINT ["/usr/bin/app-entrypoint.sh"]
VOLUME ["/app"]
WORKDIR /app/repo

CMD ["nodemon", "app.js"]
