# GitLab WebIDE server

This component is used to expose the filesystem over a basic REST endpoints.

  * POST to create a file
  * DELETE to delete a file
  * GET to read a file content
  * PUT to edit a file

We will bundle this server along with the runtime to wire the editor to the working copy.
