#!/bin/sh

REPO_NAME=repo
REPO_PATH="/export/${REPO_NAME}"

if [ -f "${REPO_PATH}" ]; then
  echo $REPO_PATH exists! Aborting!
  exit -1
fi

cd /export
git clone "${REPO_URL}" "${REPO_NAME}"

exec "$@"
