require('dotenv').config()

const express = require('express')
const app = express()

app.get('/', function (req, res) {
  res.send('Hello from GitLab WebIDE!')
})

app.listen(process.env.PORT, function () {
  console.log(`Listening to ${process.env.PORT}`)
})
